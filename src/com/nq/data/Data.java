package com.nq.data;

import java.util.ArrayList;
import java.util.List;

public class Data {
    
	public List<Double> attribute;
	public String label;
	
	public Data() {
		this.attribute = new ArrayList<>();
		this.label = null;
	}
	
	public Data(String label) {
		this.attribute = new ArrayList<>();
		this.label = label;
	}
	
	public Data(List<Double> attribute, String label){
		this.attribute = attribute;
		this.label = label;
	}
	
	public String toString(){
		StringBuffer stringBuffer = new StringBuffer();
		for(Double attr:this.attribute)
			stringBuffer.append(attr + ",");
		if(this.label!=null)
			stringBuffer.append(this.label + ";");
		else
			stringBuffer.setLength(stringBuffer.length()-1);
		return stringBuffer.toString();
	}
	
	public static Data getByLabel(List<Data> datas, String label){
		for(Data data:datas){
			if(data!=null && data.label.equals(label))
				return data;
		}
		return new Data(new ArrayList<Double>(), label);
	}
	
}