package com.nq.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Centroid {
    
	public List<Double> position;
	public String label;
	public Map<String, Integer> member;
	
	public Centroid() {
		this.position = new ArrayList<Double>();
		this.member = new HashMap<String, Integer>();
		this.label = null;
	}
	
	public Centroid(String label) {
		this.position = new ArrayList<>();
		this.member = new HashMap<String, Integer>();
		this.label = label;
	}
	
	public String toString(){
		StringBuffer stringBuffer = new StringBuffer();
		for(Double attr:this.position)
			stringBuffer.append(attr + ",");
		for(String key:this.member.keySet())
			stringBuffer.append(key +":"+ this.member.get(key) + "/");
		stringBuffer.append(this.label + ";");
		return stringBuffer.toString();
	}
	
	public String getInfo(){
		int count = 0;
		StringBuffer memberDetail = new StringBuffer();
		memberDetail.append(" {");
		for(String key:this.member.keySet()){
			count += this.member.get(key);
			memberDetail.append(key + ": " + this.member.get(key) + ", ");
		}
		memberDetail.setLength(memberDetail.length()-2);
		memberDetail.append("}");
		return this.label + " total member: " + count + memberDetail.toString(); 
	}
	
	public void joinMember(String label){
		if(this.member.containsKey(label)){
			Integer count = member.get(label) + 1;
			this.member.replace(label, count);
		} else
			this.member.put(label, 1);
	}
	
	public void putMember(String label, Integer count){
		if(this.member.containsKey(label)){
			this.member.replace(label, count);
		} else
			this.member.put(label, count);
	}
	
	public int getMemberCount(String label){
		if(this.member.containsKey(label))
			return member.get(label);
		else
			return 0;
	}
	
	public int getMemberCount(){
		int count = 0;
		for(String key:this.member.keySet())
			count += this.member.get(key);
		return count;
	}
	
	public static Centroid getByLabel(List<Centroid> centroids, String label){
		for(Centroid centroid:centroids){
			if(centroid!=null && centroid.position!=null && centroid.label.equals(label))
				return centroid;
		}
		return new Centroid(label);																					// untuk antisipasi jumlah cluster dinamis, jika centroid belum ada maka buat baru
	}
	
}