package com.nq.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.nq.utils.Formatter;
import com.nq.utils.Logger;
import com.nq.utils.Writer;

public class Result {
    
	public static String TRAIN_SUPERVISED = "Supervised Training";
	public static String TRAIN_UNSUPERVISED = "Unsupervised Training";
	public static String TEST_LVQ = "LVQ Testing";

	public List<Centroid> initialCentroids, centroids;
	public Date startTime, finishTime;
	public int iteration;
	
	private String dataset, resultFile, method;
	private double data = 0;
	private double TP[], TN[], FP[], FN[];
	
	private double accuracy, errorRate, precissionMicro, recallMicro, fscoreMicro, precissionMacro, recallMacro, fscoreMacro;
	private double fscore;
	private double specificity, aUc; 
	
	public Result(String dataset, String method, String resultFile){
		this.dataset = dataset;
		this.resultFile = resultFile;
		this.method = method;
		this.centroids = new ArrayList<Centroid>();
		this.initialCentroids = new ArrayList<Centroid>();
	}
	
	public Result() {
		this.centroids = new ArrayList<Centroid>();
	}
	
	public double getAccuracy() {
		return accuracy;
	}
	
	public boolean write(){
		boolean ok = true;
		if(analyze()){
			Writer writer = null;
			try {
				writer = new Writer(resultFile);
				writer.writeln(method);
				writer.writeln("Start time: " + Formatter.formatTime(startTime));
				writer.writeln("Finish time: " + Formatter.formatTime(finishTime));
				writer.writeln("Duration: " + Formatter.duration(Math.abs(finishTime.getTime() - startTime.getTime())));
				writer.writeln("");
				writer.writeln("Dataset: " + dataset);
				writer.writeln("Number of Data: " + Formatter.formatInt(data));
				writer.writeln("Number of Centroid: " + centroids.size());
				writer.writeln("Number of iteration: " + iteration);
				writer.writeln("");
				writer.writeln("Accuracy: " + Formatter.formatTwo(accuracy));
				writer.writeln("Error Rate: " + Formatter.formatTwo(errorRate));
				writer.writeln("Fscore " + Formatter.formatTwo(fscore));
				writer.writeln("Precission (Micro): " + Formatter.formatTwo(precissionMicro));
				writer.writeln("Recall (Micro): " + Formatter.formatTwo(recallMicro));
				writer.writeln("Fscore (Micro): " + Formatter.formatTwo(fscoreMicro));
				writer.writeln("Precission (Macro): " + Formatter.formatTwo(precissionMacro));
				writer.writeln("Recall (Macro): " + Formatter.formatTwo(recallMacro));
				writer.writeln("Fscore (Macro): " + Formatter.formatTwo(fscoreMacro));
				writer.writeln("Specificity: " + Formatter.formatTwo(specificity));
				writer.writeln("AUC: " + Formatter.formatTwo(aUc));
				if(initialCentroids!=null && initialCentroids.size()>0){
					writer.writeln("");
					writer.writeln("Initial Centroid Position:");
					for(Centroid centroid: initialCentroids)
						writer.writeln(centroid);
				}
				if(centroids!=null && centroids.size()>0){
					writer.writeln("");
					writer.writeln("Last Centroid Position:");
					for(int i=0;i<centroids.size();i++){
						Centroid centroid = centroids.get(i);
						writer.writeln("Centroid: " + centroid.label);
						writer.writeln("TP: " + Formatter.formatInt(TP[i]));
						writer.writeln("TN: " + Formatter.formatInt(TN[i]));
						writer.writeln("FP: " + Formatter.formatInt(FP[i]));
						writer.writeln("FN: " + Formatter.formatInt(FN[i]));
						writer.writeln(centroid);
					}
				}
			} catch (IOException e) {
				ok = false;
				Logger.exception(e.getMessage());
			}  finally {
				try {
					writer.close();
				} catch (IOException e) {
					ok = false;
					Logger.exception(e.getMessage());
				}
			}
		} else
			ok = false;
		return ok;
	}
	
	public boolean analyze(){																							// belum bisa multiclass --> cuma 2 cluster = benign atau botnet
		boolean ok = true;
		if(centroids!=null && centroids.size()>0){
			int l = centroids.size();
			TP = new double[l];
			TN = new double[l];
			FP = new double[l];
			FN = new double[l];
			
//			GET TP, TN, FP, FN
			for(int i=0;i<l;i++){
				Centroid centroid = centroids.get(i);
				Set<String> members = centroid.member.keySet();
				for(String member:members){
					if(member.equals(centroid.label)){
						if(member.startsWith("benign") || member.startsWith("normal"))
							TN[i] = centroid.member.get(member);
						else
							TP[i] = centroid.member.get(member);
					} else {
						if(member.startsWith("benign") || member.startsWith("normal"))
							FP[i] = centroid.member.get(member);
						else
							FN[i] = centroid.member.get(member);
					}
				}
				data = data + TP[i] + TN[i] + FP[i] + FN[i];
			}
			
//			DO ANALYSIS
			double tp=0, tn=0, fp=0, fn=0;
//			double tpfp=0, tnfp=0, tpfn=0;
			double beta = 1;
			for(int i=0;i<l;i++){
				accuracy = accuracy + ((TP[i] + TN[i]) / (TP[i] + FN[i] + FP[i] + TN[i]));
				errorRate = errorRate + ((FP[i] + FN[i]) / (TP[i] + FN[i] + FP[i] + TN[i]));
				tp = tp + TP[i];
				tn = tn + TN[i];
				fp = fp + FP[i];
				fn = fn + FN[i];
//				tpfp = tpfp + (TP[i] + FP[i]);
//				tnfp = tnfp + (TN[i] + FP[i]);
//				tpfn = tpfn + (TP[i] + FN[i]);
				if(TP[i] + FP[i] != 0)
					precissionMacro = precissionMacro + (TP[i] / (TP[i] + FP[i]));
				if(TP[i] + FN[i] != 0)
					recallMacro = recallMacro + (TP[i] / (TP[i] + FN[i]));
			}
			accuracy = accuracy/l;
			errorRate = errorRate/l;
			precissionMicro = tp/(tp + fp);
			recallMicro = tp/(tp + fn);
			fscoreMicro = ((Math.pow(beta, 2) + 1) * precissionMicro * recallMicro)/(Math.pow(beta, 2) * precissionMicro + recallMicro);			// betha=1 -> harmonic mean between precision and recall -> http://www.marcelonet.com/snippets/machine-learning/evaluation-metrix/f-beta-score
			precissionMacro = precissionMacro/l;
			recallMacro = recallMacro/l;
			fscoreMacro = ((Math.pow(beta, 2) + 1) * precissionMacro * recallMacro)/(Math.pow(beta, 2) * precissionMacro + recallMacro);			// betha=1 -> harmonic mean between precision and recall -> http://www.marcelonet.com/snippets/machine-learning/evaluation-metrix/f-beta-score
			// for binary classification only
			fscore = ((Math.pow(beta, 2) + 1) * tp) / (((Math.pow(beta, 2) + 1) * tp) + (Math.pow(beta, 2) * fn) + fp);
			specificity = tn/(fp + tn);
			aUc = 0.5 *((tp/(tp + fn)) + (tn/(tn + fp)));
		} else
			ok = false;
		return ok;
	}
}
