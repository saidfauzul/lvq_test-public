package com.nq.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.nq.utils.FileName;
import com.nq.utils.Logger;
import com.nq.utils.Reader;
import com.nq.utils.Writer;

public class Sampling {
	String src, label;
	int count;
	
	public Sampling(String src, int count, String label){
		this.src = src;
		this.label = label;
		this.count = count;
	}
	
	public Sampling(String src, int count){
		this.src = src;
		this.count = count;
	}
	
	public boolean start(){
		boolean ok = true;
		
		if(src.indexOf('*')>=0) {
			try {
				String[] files = FileName.getWildcardFiles(src);
				for(String file:files) {
					Logger.info("get sample from: " + file);
					ok = sampling(file);
				}
			} catch (IOException e) {
				ok = false;
				Logger.error("can't get wildcard files");
				Logger.exception(e.getMessage());
			}
		} else
			ok = sampling(src);
		
		return ok;
	}
	
	public boolean sampling(String file) {
		boolean ok = true;
		Reader reader = null;
		Writer writer = null;
		
		List<String> buffer0 = new ArrayList<String>();																		// antisipasi jumlah data maksimal lebih dari Integer.MAX_VALUE
		List<String> buffer1 = new ArrayList<String>();
		
		try {
			reader = new Reader(file);
			while(true){
				String line = reader.readln();
				if(line==null)
					break;
				else if(line.startsWith("MI"))																				// by pass header
					continue;
				
				if(label!=null)
					line = line + "," + label + ";";
				
				if(buffer0.size()<Integer.MAX_VALUE)
					buffer0.add(line);
				else
					buffer1.add(line);
			}
			
			if(count>0) {
//				GET SAMPLE
				
				if(count>(buffer0.size() + buffer1.size())){
					Logger.error("not enough data [" + (buffer0.size() + buffer1.size()) + "] for sampling [" + count + "]");
					return false;
				}
				
				writer = new Writer(FileName.sampling(file));
				if(buffer1.size()==0){
					for(int i=0;i<count;i++){
						String line = buffer0.get(getRandom(buffer0.size()));
						writer.writeln(line);
					}
				} else {
					Logger.info("not enough buffer, use the next buffer");
					int maxBuffer0 = buffer0.size();
					int maxBuffer1 = buffer1.size();
					int count0 = count * (maxBuffer0 / (maxBuffer0 + maxBuffer1));											// make proporsional
					int count1 = count * (1 - (maxBuffer0 / (maxBuffer0 + maxBuffer1)));
					
					for(int i=0;i<count0;i++){
						String line = buffer0.get(getRandom(maxBuffer0));
						writer.writeln(line);
					}
					
					for(int i=0;i<count1;i++){
						String line = buffer1.get(getRandom(maxBuffer1));
						writer.writeln(line);
					}
				}
			} else {																										// take all
				writer = new Writer(FileName.sampling(file));
				for(String line:buffer0)
					writer.writeln(line);
				for(String line:buffer1)
					writer.writeln(line);
			}
		} catch (IOException e) {
			ok = false;
			Logger.exception(e.getMessage());
		} finally {
			try {
				writer.close();
				reader.close();
			} catch (IOException e) {
				ok = false;
				Logger.exception(e.getMessage());
			}
		}
		return ok;
	}
	
	public int getRandom(int max){
		double randomDouble = Math.random();
		randomDouble = randomDouble * max;
		int randomInt = (int) randomDouble;
		return randomInt;
	}
	
}
