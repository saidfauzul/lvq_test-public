package com.nq.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.nq.utils.Logger;
import com.nq.data.Centroid;
import com.nq.data.Data;
import com.nq.data.Result;
import com.nq.utils.Clustering;
import com.nq.utils.FileName;
import com.nq.utils.Formatter;
import com.nq.utils.Reader;
import com.nq.utils.Writer;

public class Training {
	
	int OPIMIZATION = 7;
	
	String dataset;
	int clusterCount;
	boolean supervised;
	private Stat stat;
	private Result result;
	
	public Training(String dataset, int clusterCount, boolean supervised){
		this.dataset = dataset;
		this.clusterCount = clusterCount;
		this.supervised = supervised;
		
	}
	
	public boolean start(){
		boolean ok = true;
		stat = new Stat(dataset, true);
		Logger.info("get statistics info");
		if(!stat.calculate()){																														// setiap training, lakukan perhitungan statistik --> setiap learning baca file statistik
			Logger.error("calculate statistics failed");
			return false;
		}
		
		Writer writer = null;
		try {
			writer = new Writer(FileName.trainingCent(dataset, true));
			List<Centroid> centroids = null;
			
			if(supervised) {
				result = new Result(dataset, Result.TRAIN_SUPERVISED, FileName.trainingResult(dataset));
				Logger.info("supervised training initialized");
				result.startTime = new Date();
				centroids = average();
			} else {
				result = new Result(dataset, Result.TRAIN_UNSUPERVISED, FileName.trainingResult(dataset));
				Logger.info("unsupervised training initialized");
				result.startTime = new Date();
				
				List<List<Centroid>> checkCents = new ArrayList<>();
				for(int i=0;i<OPIMIZATION;i++){																												// optimalization k-means = run 7 times
					Logger.info("start k-means optimization iteration: " + i);
					List<Centroid> checkCent = kmeans(0);
					if(checkCent!=null)
						checkCents.add(checkCent);
				}
				
//				CHECK THE LIST WITH OPTIMUM RESULT
				
//				get by highest accuracy
				
				double[] accuracy = new double[checkCents.size()];
				for(int i=0;i<checkCents.size();i++) {
					List<Centroid> cents = checkCents.get(i);
					Result result = new Result();
					result.centroids=cents;
					if(result.analyze()) {
						accuracy[i] = result.getAccuracy();
						Logger.debug("training optimization iteration-" + i + " accuracy: " + Formatter.formatFour(accuracy[i]));
					} else {
						Logger.debug("result analysis for training optimization iteration-" + i + " failed");
					}
				}
				
				double highestAccuracy=0;
				for(int i=0;i<accuracy.length;i++) {
					if(highestAccuracy<accuracy[i]) {
						highestAccuracy = accuracy[i];
						centroids = checkCents.get(i);
					}
				}
				
//				get by similarity result
				
//				List<String[]> checker = new ArrayList<>();
//				List<Integer> checkerCounter = new ArrayList<>();
//				List<Integer> checkerIndex = new ArrayList<>();
//				for(int i=0;i<checkCents.size();i++){
//					List<Centroid> checkCent = checkCents.get(i);
//					if(i==0){
////						INSERT CHECKER FOR FIRST TIME
//						String[] str = new String[checkCent.size()];
//						for(int j=0;j<str.length;j++){
//							Centroid cent = checkCent.get(j); 
//							str[j] = cent.label + ":" + cent.getMemberCount();
//						}
//						checker.add(str);
//						checkerCounter.add(1);
//						checkerIndex.add(i);
//					} else {
//						String[] str = new String[checkCent.size()];
//						for(int j=0;j<str.length;j++){
//							Centroid cent = checkCent.get(j); 
//							str[j] = cent.label + ":" + cent.getMemberCount();
//						}
//						
////						CHECK IDENTICAL
//						boolean identic = false;
//						int similarity = 0;
//						int indexIdentic = 0;
//						for(int j=0;j<checker.size();j++){
//							String[] check = checker.get(j);																					// posisi checker yang di cek
//							if(check.length==str.length){
//								for(int k=0;k<check.length;k++){																			// bubble check
//									for(int l=0;l<str.length;l++){
//										if(check[k].equals(str[l]))
//											similarity++;
//									}
//								}
//								if(similarity==check.length){																					// dinyatakan sama, jika jumlah kesamaan = check.length --> karena pakai bubble check
//									indexIdentic = j;
//									identic = true;
//									break;																													// kalau sudah ketemu yang sama, hentikan looping
//								}
//							}
//						}
//						
////						UPDATE CHECKER
//						if(identic){
//							checkerCounter.set(indexIdentic, checkerCounter.get(indexIdentic)+1);					// posisi checkerCount yg ditambah = posisi checker yang di cek
//						} else {																																// kalau tidak ada yang sama, tambahkan dalam list checker
//							checker.add(str);																											// add checker, checkerCounter, checkerIndex bersamaan, supaya index dalam list-nya sama
//							checkerCounter.add(1);
//							checkerIndex.add(i);																									// checkerIndex menyimpan index yang asli dari List of List of Centroid
//						}
//					}
//				}
//				
////				GET CENTROIDS WIDELY IDENTICAL
//				int largest = 0;
//				int centroidsIndex = 0;
//				for(int i=0;i<checkerCounter.size();i++){
//					Logger.info("checkerCounter: [" + checker.get(i)[0] + ", " + checker.get(i)[1] + "] -> " +  checkerCounter.get(i));
//					if(i==0)
//						largest = checkerCounter.get(i);
//					else if(largest<checkerCounter.get(i)){
//						largest = checkerCounter.get(i);
//						centroidsIndex = checkerIndex.get(i);
//					}
//				}
//				centroids = checkCents.get(centroidsIndex);
			}
			
			if(centroids!=null){																														// writer the training results
				for(Centroid centroid:centroids)
					writer.writeln(centroid);
				
				result.centroids = centroids;
				result.finishTime = new Date();
				result.write();
			}
		} catch (IOException e) {
			Logger.exception(e.getMessage());
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				Logger.exception(e.getMessage());
			}
		}
		return ok;
	}
	
	private List<Centroid> average(){
		List<Centroid> finalCentroids = new ArrayList<>();
		Reader reader = null;
		try {
			reader = new Reader(dataset);
			List<Centroid> centroids = new ArrayList<>();
			while(true){
				Data data = reader.readDataln();
				if(data!=null){
					data.attribute = Norm.normalize(data.attribute, stat.means, stat.stds);						// normalizing data
					Centroid centroid = Centroid.getByLabel(centroids, data.label);									// create centroid sejumlah label --> kalau K-Means jumlah centroid ditentukan
					int centroidIndex = centroids.indexOf(centroid);
					List<Double> centroidPos = centroid.position;
					for(int i=0;i<data.attribute.size();i++){
						Double sum = data.attribute.get(i);
						if(centroidPos.size()<data.attribute.size()){
							centroidPos.add(sum);
						} else {
							sum = centroidPos.get(i) + data.attribute.get(i);
							centroidPos.set(i, sum);
						}
					}
					centroid.joinMember(data.label);
					if(centroid.getMemberCount()==1)																				// first member of new centroid
						centroids.add(centroid);
					else
						centroids.set(centroidIndex, centroid);
				} else
					break;
			}
			for(Centroid centroid:centroids){																							// get average
				List<Double> position = centroid.position;
				int memberSize = centroid.getMemberCount();
				if(memberSize>0){
					for(int i=0;i<position.size();i++){
						Double attr = position.get(i)/memberSize;
						position.set(i, attr);
					}
					centroid.position = position;
					finalCentroids.add(centroid);
					Logger.info(centroid.label + ": " + centroid.toString());
				}
			}
		} catch (IOException e) {
			Logger.exception(e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				Logger.exception(e.getMessage());
			}
		}
		return finalCentroids;
	}
	
	List<Centroid> centroids = null;
	private List<Centroid> kmeans(int iteration){
		if(iteration==0){																															// reset centroids for k-means optimization
			centroids = null;
			result.iteration = 0;
		}
		Logger.info("start kmeans iteration-" + iteration);
		Reader reader = null;
		Data data = null;
		List<Centroid> newCentroids = new ArrayList<>();
		
		try {
//			CENTROID INITIALIZATION
			if(centroids==null){																												// prepare centroid --> pada iterasi-0 centroid masih null
				centroids = new ArrayList<>();
				Logger.info("initial random centroid");
				reader = new Reader(dataset);
				data = reader.readDataln();																									// first data used for get length of attribute
				for(int i=0;i<clusterCount;i++){
					Centroid centroid = new Centroid();																				// generate centroid
					centroid.position = Clustering.generateCentroid(data.attribute.size());
					centroid.label = "cent-" + i;
					centroids.add(centroid);
					Logger.info("new centroid: " + centroid.toString());
				}
				reader.close();																														// close reader for reuse
			
				Logger.info("initial random centroid normalized");
				centroids = Norm.normalizeRandomCentroids(centroids);												// normalized random centroid using random z-score
				for(Centroid centroid:centroids){
//					centroid.position = Norm.normalize(centroid.position, stat.means, stat.stds);				// normalized random centroid using dataset z-score
					Logger.info(centroid.toString());
				}
				result.initialCentroids = centroids;
			}
		
//			INITIATE NEW CENTROID
			int attrLength = centroids.get(0).position.size();
			for(int i=0;i<clusterCount;i++){
				Centroid newCentroid = new Centroid();																			// initialized new centroid position
				newCentroid.position = Clustering.zeroCentroid(attrLength);
				newCentroid.label = "cent-" + i;
				newCentroids.add(newCentroid);
			}
		
//			GET DISTANCE
			reader = new Reader(dataset);
			while(true){
				data = reader.readDataln();	
				if(data==null){
					reader.close();
					break;																																// assume end of file
				}
				
				data.attribute = Norm.normalize(data.attribute, stat.means, stat.stds);							// normalizing data
				
				double[] distance = new double[clusterCount];
				for(int i=0;i<clusterCount;i++){
					Centroid centroid = centroids.get(i);
					distance[i] = Clustering.distance(data.attribute, centroid.position);
				}
				
//				CHECK NEAREST
				boolean first = true;
				int nearestIndex = 0;
				double nearest = 0;
				for(int i=0;i<clusterCount;i++){																							// check nearest centroid
					if(first){
						first = false;
						nearest = distance[i];
						nearestIndex = i;
					} else {
						if(distance[i] < nearest){
							nearest = distance[i];
							nearestIndex = i;
						}
					}
				}
				
//				PREPARE NEW CENTROID POSSITION
				Centroid newCentroid = newCentroids.get(nearestIndex);
				for(int i=0;i<data.attribute.size();i++){
					double sum = newCentroid.position.get(i);
					sum = sum + data.attribute.get(i);																					// new centroid position = average all data in cluster
					newCentroid.position.set(i, sum);
				}
				newCentroid.joinMember(data.label);
				newCentroids.set(nearestIndex, newCentroid);
			}
			
			Logger.info("centroid members");
			for(Centroid newCentroid:newCentroids)
				Logger.info(newCentroid.label + " total member: " + newCentroid.getMemberCount() + " " + newCentroid.member.toString());
			
//			CALCULATE NEW CENTROID POSISITION
			for(int i=0;i<clusterCount;i++){
				Centroid newCentroid = newCentroids.get(i);
				List<Double> position = newCentroid.position;
				int memberCount = newCentroid.getMemberCount();
				for(int j=0;j<position.size();j++){
					if(memberCount>0){
						Double attr = position.get(j)/memberCount;
						position.set(j, attr);
					} else
						position.set(j, Double.valueOf(0));
				}
				
				Logger.info("new centroid position");
				Logger.info(newCentroid.toString());
				newCentroid.position = position;
				newCentroids.set(i, newCentroid);
			}
			
//			CHECK IF CENTROID MOVED
			boolean fix = true;
			for(int i=0;i<clusterCount;i++){
				if(fix){
					Centroid centroid = centroids.get(i);
					Centroid newCentroid = newCentroids.get(i);
					for(int j=0;j<centroid.position.size();j++){
						if(fix && !centroid.position.get(j).equals(newCentroid.position.get(j))){
							fix = false;
							Logger.info("centroid moved");
							break;
						}
					}
				} else
					break;
			}
			
//			LABEL ASSIGNMENT
			if(fix){																																		// assign label
				for(int i=0;i<clusterCount;i++){
					Centroid centroid = newCentroids.get(i);
					String lastLabel = centroid.label;
					Set<String> labels = centroid.member.keySet();
					Integer larger = 0;
					for(String label:labels){
						Integer memberCount = centroid.member.get(label);	
						if(larger<memberCount){
							larger = memberCount;
							centroid.label = label;
						}
					}
					Logger.info("Centroid " + lastLabel + " assign to " + centroid.label);
					newCentroids.set(i, centroid);
				}
				result.iteration = iteration + 1;
				Logger.info("total iteration: " + (iteration+1));
			} else {
				centroids = newCentroids;																									// digunakan sebagai centroid awal pada iterasi berikutnya
				newCentroids = kmeans(iteration+1);																				// if centroid still move, reprocess
			}
		} catch (IOException e) {
			Logger.exception(e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				Logger.exception(e.getMessage());
			}
		}
		return newCentroids;
	}
}
