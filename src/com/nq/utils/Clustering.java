package com.nq.utils;

import java.util.ArrayList;
import java.util.List;

public class Clustering {
    
    public static List<Double> generateCentroid(int lengthAttribute){
    		List<Double> centroid = new ArrayList<Double>();
    		for (int i=0;i<lengthAttribute;i++){
    			centroid.add(Math.random());
    		}
    		return centroid;
	}
    
    public static List<Double> zeroCentroid(int lengthAttribute){
		List<Double> centroid = new ArrayList<Double>();
		for (int i=0;i<lengthAttribute;i++){
			centroid.add(Double.valueOf(0));
		}
		return centroid;
	}

    public static double distance(List<Double> data1, List<Double> data2) {
        double distance = 0;
        for (int i=0;i<data2.size();i++) {
            distance += Math.pow(data1.get(i)-data2.get(i), 2);
        }
        return (Math.sqrt(distance));
    }

}
