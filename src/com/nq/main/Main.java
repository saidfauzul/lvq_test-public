package com.nq.main;

import com.nq.utils.Logger;
import com.nq.utils.Timer;

public class Main {
	
	public static void main(String args[]) {
		
		Logger.startApp();
		if(args.length<=1 || !args[0].startsWith("--")){
			Logger.warning("invalid arguments: [--function], [arg0], [arg1], ..");
			info();
		} else if(args[0].equals("--copy")){
			Timer timer = new Timer();
			try {
				int startLine = Integer.valueOf(args[3]).intValue();
				int endLine = Integer.valueOf(args[4]).intValue();
				Copy copier = null;
				if(args.length==7)
					copier = new Copy(args[1], args[2], startLine, endLine, Boolean.valueOf(args[5]).booleanValue(), args[6]);
				else
					copier = new Copy(args[1], args[2], startLine, endLine, Boolean.valueOf(args[5]).booleanValue());
				Logger.info("start copying file: " + args[1]);
				if(copier.start())
					Logger.info("copying file succeeded", timer.duration());
				else
					Logger.error("copying file failed", timer.duration());
			} catch (Exception e) {
				Logger.exception(e.getMessage());
				Logger.error("invalid arguments for copy function, args: String src, String dest, int startLine, int endLine, boolean append, [String label]");
			}
		} else if(args[0].equals("--sampling")){
			Timer timer = new Timer();
			int count = Integer.valueOf(args[2]).intValue();
			try {
				Sampling sampling = null;
				if(args.length==3)
					sampling = new Sampling(args[1], count);
				else
					sampling = new Sampling(args[1], count, args[3]);
				
				Logger.info("start random sampling: " + args[1]);
				if(sampling.start())
					Logger.info("random sampling succeeded", timer.duration());
				else
					Logger.error("random sampling failed", timer.duration());
			} catch (Exception e) {
				Logger.exception(e.getMessage());
				Logger.error("invalid arguments for sampling function, args: String dataset, Int count, [String label]");
			}
		}  else if(args[0].equals("--merge")){
			Timer timer = new Timer();
			try {
				Merge merge = new Merge(args);
				Logger.info("start merge to: " + args[1]);
				if(merge.start())
					Logger.info("merge file succeeded", timer.duration());
				else
					Logger.error("merge file failed", timer.duration());
			} catch (Exception e) {
				Logger.exception(e.getMessage());
				Logger.error("invalid arguments for merge function, args: String dest, String[] src");
			}
		} else if(args[0].equals("--training")){
			Timer timer = new Timer();
			int clusterCount = Integer.valueOf(args[2]).intValue();
			boolean supervised = Boolean.valueOf(args[3]).booleanValue();
			try {
				Training training = new Training(args[1], clusterCount, supervised);
				Logger.info("start training data: " + args[1]);
				if(training.start())
					Logger.info((supervised?"supervised":"unsupervised") + " training succeeded", timer.duration());
				else
					Logger.error((supervised?"supervised":"unsupervised") + " training failed", timer.duration());
			} catch (Exception e) {
				Logger.exception(e.getMessage());
				Logger.error("invalid arguments for training function, args: String dataset, Int clusterCount, boolean supervised");
			}
		} else if(args[0].equals("--learning")){
			Timer timer = new Timer();
			Double learningRate = Double.valueOf(args[2]);
			try {
				LVQ lvq = new LVQ(args[1], learningRate);
				Logger.info("start learning: " + args[1]);
				if(lvq.start())
					Logger.info("learning finished", timer.duration());
				else
					Logger.error("learning failed", timer.duration());
			} catch (Exception e) {
				Logger.exception(e.getMessage());
				Logger.error("invalid arguments for learning function, args: String dataset, double learning rate");
			}
		}
		Logger.endApp();
		System.exit(0);
	}
	
	public static void info(){
		StringBuffer info = new StringBuffer();
		info.append("available function\n");
		info.append("--copy, args: source, destination, start line, end line, boolean append, [label]\n");
		info.append("--sampling, args: dataset, int number of sample, [label]\n");
		info.append("--merge, args: destination, source[0], source[1], source[..]\n");
		info.append("--training, args: dataset, number of cluster, boolean supervised\n");
		info.append("--learning, args: dataset, learning rate\n");
		Logger.info(info.toString());
		return;
	}
}
