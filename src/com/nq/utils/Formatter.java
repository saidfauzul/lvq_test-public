package com.nq.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
	 private static DateFormat timeFormat = new SimpleDateFormat("yyy/MM/dd hh:mm");
	 private static DateFormat dateFormat = new SimpleDateFormat("yyyMMdd");
	 private static DecimalFormat formatTwo = new DecimalFormat("#0.00");
	 private static DecimalFormat formatFour = new DecimalFormat("#0.0000");
	 private static DecimalFormat formatInt = new DecimalFormat("#");
	 
	 public static String formatTime(Date date){
		 return timeFormat.format(date);
	 }
	 
	 public static String formatDate(Date date){
		 return dateFormat.format(date);
	 }
	 
	 public static String formatTwo(Double value){
		 return formatTwo.format(value);
	 }
	 
	 public static String formatFour(Double value){
		 return formatFour.format(value);
	 }
	 
	 public static String formatInt(Double value){
		 return formatInt.format(value);
	 }
	 
	 public static String duration(long timemilis){
        long miliSecond = timemilis % 1000;
        long tempSecond = (timemilis - miliSecond) / 1000;
        long tempMinute;
        long second=0;
        long minute=0;
        long hour=0;
        
        if (tempSecond >= 60){
            second = tempSecond % 60; 
            tempMinute = (tempSecond - second) / 60;
            if (tempMinute >= 60){
                minute = tempMinute % 60;
                hour = (tempMinute - minute) / 60;
            }
            else
                minute = tempMinute;
        }
        else
            second = tempSecond;
        
        return (hour+":"+minute+":"+second+":"+miliSecond);
	 }
}