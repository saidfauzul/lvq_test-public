#!/bin/sh

SECONDS=0

declare app=build/jar/nq_lvq.jar
declare basedir=/Users/q/Documents/mte/random_process/jurnal/datasets/BaIoT

declare -a devices=("Danmini_Doorbell" "Ecobee_Thermostat" "Ennio_Doorbell" "Philips_B120N10_Baby_Monitor" "Provision_PT_737E_Security_Camera" "Provision_PT_838_Security_Camera" "Samsung_SNH_1011_N_Webcam" "SimpleHome_XCS7_1002_WHT_Security_Camera" "SimpleHome_XCS7_1003_WHT_Security_Camera")
declare -a numOfBenignSamples=(40000 10000 20000 100000 50000 70000 40000 30000 10000)
declare -a numOfAttackSamples=(4000 1000 4000 10000 5000 7000 8000 3000 1000)

#declare -a devices=("Danmini_Doorbell")

declare -a attacks=("gafgyt_attacks" "mirai_attacks")

len=${#devices[@]}
for (( i=0; i<$len; i++ ))
do
	device=${devices[$i]}
	numOfBenignSample=${numOfBenignSamples[$i]}
	numOfAttackSample=${numOfAttackSamples[$i]}
	
	echo processing $basedir/$device
	declare learn=$basedir/$device/learn
	if [ -d $learn ]
	then
		rm -r $learn
	fi
	mkdir $learn
	mkdir $learn/training-raw
	mkdir $learn/learning-raw
	
	# sampling
	# data training
	java -jar $app --sampling "$basedir/$device/*.csv" $numOfBenignSample benign
	mv $basedir/$device/*.data $learn/training-raw/
	# data learning
	java -jar $app --sampling "$basedir/$device/*.csv" 0 benign
	mv $basedir/$device/*.data $learn/learning-raw/
	
	for attack in "${attacks[@]}"
	do
		# nggak semua perangkat terinfeksi mirai
		if [ -d $basedir/$device/$attack ]
		then
			# data training
			java -jar $app --sampling "$basedir/$device/$attack/*.csv" $numOfAttackSample attack
			
			# perlu rename karena ada nama file yang sama
			for name in $basedir/$device/$attack/*.data
			do
	    		newname=${name%.data}_$attack.data
	    		mv $name $newname
			done
			mv $basedir/$device/$attack/*.data $learn/training-raw/
					
			# data learning
			java -jar $app --sampling "$basedir/$device/$attack/*.csv" 0 attack
			for name in $basedir/$device/$attack/*.data
			do
    			newname=${name%.data}_$attack.data
    			mv $name $newname
			done
			mv $basedir/$device/$attack/*.data $learn/learning-raw/
		fi
	done
	
	# merge
	java -jar $app --merge $learn/training.data $learn/training-raw/*.data
	java -jar $app --merge $learn/learning.data $learn/learning-raw/*.data
	
	# training
	java -jar $app --training $learn/training.data 2 false
	
	# learning
	for lr in 0.1 0.01 0.001 0.0001
	do
		java -jar $app --learning $learn/learning.data $lr
	done
	
done

duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."

exit 0