package com.nq.main;

import java.util.ArrayList;
import java.util.List;

import com.nq.data.Centroid;

public class Norm {
	
	public static List<Double> normalize(List<Double> attribute, List<Double> mean, List<Double> std){
		List<Double> normal = new ArrayList<>();
		for(int i=0;i<attribute.size();i++){
			Double norm = (attribute.get(i)-mean.get(i))/std.get(i);					// z-score
			normal.add(norm);
		}
		return normal;
	}
	
	private static List<List<Double>> normalizeRandom(List<List<Double>> randoms){
		int n=0;
		List<Double> means = new ArrayList<>();
		for(List<Double> attributes:randoms){													// count sum for prepare mean
			n++;
			for(int i=0;i<attributes.size();i++){
				Double sum = attributes.get(i);
				if(means.size()<attributes.size()){
					means.add(sum);
				} else {
					sum = means.get(i) + attributes.get(i);
					means.set(i, sum);
				}
			}
		}
		
		for(int i=0;i<means.size();i++){																// get mean
			Double mean = means.get(i)/n;
			means.set(i, mean);
		}
		
		List<Double> stds = new ArrayList<>();
		for(List<Double> attributes:randoms){													// count var for prepare std
			for(int i=0;i<attributes.size();i++){
				Double mean = means.get(i);
				Double var = attributes.get(i);
				if(stds.size()<attributes.size()){
					stds.add(Math.pow(var - mean, 2));
				} else {
					var = stds.get(i) + (Math.pow(var - mean, 2));
					stds.set(i, var);
				}
			}
		}
		
		for(int i=0;i<stds.size();i++){																	// get std
			Double std = Math.sqrt(stds.get(i)/n);
			stds.set(i, std);
		}

		for(int i=0;i<randoms.size();i++){															// normalizing
			List<Double> attributes = randoms.get(i);
			List<Double> normalized = new ArrayList<>();
			for(int j=0;j<attributes.size();j++){
				Double attribute = (attributes.get(j)-means.get(j))/stds.get(j);
				normalized.add(attribute);
			}
			randoms.set(i, normalized);
		}
		
		return randoms;
	}
	
	public static List<Centroid> normalizeRandomCentroids(List<Centroid> centroids){
		List<List<Double>> randoms = new ArrayList<>();
		for(Centroid centroid:centroids){
			List<Double> attribute = new ArrayList<>();
			for(Double attr:centroid.position)
				attribute.add(attr);
			randoms.add(attribute);
		}
		
		randoms = normalizeRandom(randoms);
		
		for(int i=0;i<centroids.size();i++){
			Centroid centroid = centroids.get(i);
			centroid.position = randoms.get(i);
			centroids.set(i, centroid);
		}
		
		return centroids;
	}
}
