package com.nq.utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import com.nq.utils.Writer;

public class Logger {

	private static Writer logWriter;
	
	static {
        		File log = new File("log");
        		if(!log.exists()) log.mkdir();
        		String logFile = log.getAbsolutePath() + File.separator + Formatter.formatDate(new Date()) + "_nq_lvq.log";
        		try {
				logWriter = new Writer(logFile, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public static void startApp() {
		try {
			String msg = "\n====================\tSTART: " + Formatter.formatTime(new Date()) + "\t ====================\n"; 
			logWriter.writeln(msg);
			System.out.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void endApp() {
		try {
			String msg = "\n====================\tEND: " + Formatter.formatTime(new Date()) + "\t ====================\n"; 
			logWriter.writeln(msg);
			System.out.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void info(String msg) {
		try {
			msg = "[INFO]\t"+ msg;
			logWriter.writeln(msg);
			System.out.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void warning(String msg) {
		try {
			msg = "[WARN]\t"+ msg;
			logWriter.writeln(msg);
			System.out.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void error(String msg) {
		try {
			msg = "[ERR]\t"+ msg;
			logWriter.writeln(msg);
			System.err.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void exception(String msg) {
		try {
			msg = "[EXCP]\t"+ msg;
			logWriter.writeln(msg);
			System.err.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void debug(String msg) {
		try {
			msg = "[DEBUG]\t"+ msg;
			logWriter.writeln(msg);
			System.err.println(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void info(String msg, String duration) {
		info(msg);
		info("duration: " + duration);
	}
	
	public static void warning(String msg, String duration) {
		warning(msg);
		info("duration: " + duration);
	}
	
	public static void error(String msg, String duration) {
		error(msg);
		info("duration: " + duration);
	}

}