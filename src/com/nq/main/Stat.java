package com.nq.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.nq.data.Data;
import com.nq.utils.FileName;
import com.nq.utils.Logger;
import com.nq.utils.Reader;
import com.nq.utils.Writer;

public class Stat {
	String dataset, statFile;
	List<Double> means;
	List<Double> stds;
	
	public Stat(String dataset, boolean training){
		this.dataset = dataset;
		this.statFile = FileName.statistics(dataset, training);
	}
	
	public boolean calculate(){
		boolean ok = true;
		Reader reader = null;
		Writer writer = null;
		File file = new File(statFile);
		if(file.exists()) file.delete();																				// delete statFile existing
		List<Double> sums = new ArrayList<>();
		try {
			writer = new Writer(statFile, true);
			reader = new Reader(dataset);
			int n = 0;
			while(true){
				Data data = reader.readDataln();
				if(data!=null){
					n++;
					for(int i=0;i<data.attribute.size();i++){
						Double sum = data.attribute.get(i);
						if(sums.size()<data.attribute.size()){
							sums.add(sum);
						} else {
							sum = sums.get(i) + data.attribute.get(i);
							sums.set(i, sum);
						}
					}
				} else
					break;
			}
			
			Logger.info("number of data (n): " + n);
			
			means = new ArrayList<>();
			for(Double sum:sums)
				means.add(sum/n);
			
			Data meanWrite = new Data(means, "mean");
			writer.writeln(meanWrite);
			Logger.info("mean: " + meanWrite.toString());

			List<Double> vars = new ArrayList<>();
			reader = new Reader(dataset);
			while(true){
				Data data = reader.readDataln();
				if(data!=null){
					for(int i=0;i<data.attribute.size();i++){
						Double mean = means.get(i);
						Double var = data.attribute.get(i);
						if(vars.size()<data.attribute.size()){
							vars.add(Math.pow(var - mean, 2));
						} else {
							var = vars.get(i) + (Math.pow(var - mean, 2));
							vars.set(i, var);
						}
					}
				} else
					break;
			}
			
			stds = new ArrayList<>();
			for(Double var:vars)
				stds.add(Math.sqrt(var/n));
			
			Data stdWrite = new Data(stds, "std");
			writer.writeln(stdWrite);
			Logger.info("std: " + stdWrite.toString());
			Logger.info("statistics info saved to: " + statFile);
			
		} catch (IOException e) {
			ok = false;
			Logger.exception(e.getMessage());
		} finally {
			try {
				writer.close();
				reader.close();
			} catch (IOException e) {
				ok = false;
				Logger.exception(e.getMessage());
			}
		}
		return ok;
	}
	
	public boolean read(){
		Logger.info("read statistics file: " + dataset);
		boolean ok = true;
		Reader reader = null;
		try {
			reader = new Reader(statFile);
			while(true){																										// get data statistics
				Data data = reader.readDataln();
				if(data!=null){
					if(data.label.equals("mean"))
						means = data.attribute;
					else
						stds = data.attribute;
				} else
					break;
			}
		} catch (IOException e) {
			ok = false;
			Logger.exception(e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				ok = false;
				Logger.exception(e.getMessage());
			}
		}
		return ok && this.means!=null && this.stds!=null;
	}
	
}
