package com.nq.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nq.utils.Logger;
import com.nq.data.Centroid;
import com.nq.data.Data;
import com.nq.data.Result;
import com.nq.utils.Clustering;
import com.nq.utils.FileName;
import com.nq.utils.Reader;
import com.nq.utils.Writer;

public class LVQ {
	String dataset;
	List<Centroid> centroids = new ArrayList<Centroid>();
	private Stat stat;
	private Result result;
	double learningRate;
	
	public LVQ(String dataset, Double learningRate){
		this.dataset = dataset;
		this.learningRate = learningRate;
	}
	
	public boolean start(){
		boolean ok = true;
		result = new Result(dataset, Result.TEST_LVQ, FileName.learningResult(dataset, learningRate));
		
//		READ STATISTICS
		stat = new Stat(dataset, false);
		Logger.info("get statistics info");
		if(!stat.read()){																						// setiap training, lakukan perhitungan statistik --> setiap learning baca file statistik
			Logger.error("reading statistics failed");
			return false;
		}
		
//		READ TRAINING CENTROID
		Logger.info("get training centroid");
		Reader reader = null;
		try {
			reader = new Reader(FileName.trainingCent(dataset, false));
			while(true){
				Centroid centroid = reader.readCentroidln();
				if(centroid==null)
					break;
				else
					centroids.add(centroid);
			}
		} catch (IOException e) {
			Logger.exception(e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				Logger.exception(e.getMessage());
			}
		}
		
		if(centroids.size()<=0){
			Logger.error("training centroid not found");
			return false;
		}
		
		result.initialCentroids = centroids;
		int clusterCount = centroids.size();
		
//		START TESTING
		Logger.info("start learning");
		result.startTime = new Date();
		
		try {
			reader = new Reader(dataset);
			while(true){
				Data data = reader.readDataln();
				if(data==null)
					break;
				
				data.attribute = Norm.normalize(data.attribute, stat.means, stat.stds);
				
				double[] distance = new double[clusterCount];
				for(int i=0;i<clusterCount;i++){
					Centroid centroid = centroids.get(i);
					distance[i] = Clustering.distance(data.attribute, centroid.position);
				}
				
//				CHECK NEAREST
				boolean first = true;
				int nearestIndex = 0;
				double nearest = 0;
				for(int i=0;i<clusterCount;i++){																	// check nearest centroid
					if(first){
						first = false;
						nearest = distance[i];
						nearestIndex = i;
					} else {
						if(distance[i] < nearest){
							nearest = distance[i];
							nearestIndex = i;
						}
					}
				}
				
//				MOVE CENTROID
				Centroid nearestCent = centroids.get(nearestIndex);
				nearestCent.position = newPos(nearestCent.position, data.attribute);
				nearestCent.joinMember(data.label);
				centroids.set(nearestIndex, nearestCent);
			}
			
			Writer writer = new Writer(FileName.learningCent(dataset, learningRate));
			for(Centroid centroid:centroids)
				writer.writeln(centroid);
			writer.close();
			
		} catch (IOException e) {
			ok = false;
			Logger.exception(e.getMessage());
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				ok = false;
				Logger.exception(e.getMessage());
			}
		}
		result.centroids = centroids;
		result.finishTime = new Date();
		result.write();
		return ok;
	}
	
	public List<Double> newPos (List<Double> centroidPos, List<Double> dataAttr){
		for (int i=0;i<centroidPos.size();i++){
			Double newPos = centroidPos.get(i) + ((dataAttr.get(i) - centroidPos.get(i)) * learningRate);
			centroidPos.set(i, newPos);
		}
		return (centroidPos);
    }
	
}
