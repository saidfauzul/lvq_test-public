package com.nq.utils;

import java.util.Calendar;

public class Timer {
    
    long start, duration;
    
    public Timer(){
        start = Calendar.getInstance().getTimeInMillis();
    }
    
    public String duration(){
        duration = Calendar.getInstance().getTimeInMillis() - start;
        return Formatter.duration(duration);
    }
}
