package com.nq.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import com.nq.data.Centroid;
import com.nq.data.Data;

public class Reader {
	
	private FileReader fileReader = null;
	private BufferedReader bufferedReader;

	public Reader(String file) throws FileNotFoundException {
		fileReader = new FileReader(file);
		bufferedReader = new BufferedReader(fileReader);
	}

	public String readln() throws IOException {
		return bufferedReader.readLine();
	}
	
	private String safeReadln() throws IOException {
		String line = bufferedReader.readLine();
		if(line!=null && line.isEmpty())
			line = safeReadln();
		return line;
	}
	
	public Data readDataln() throws IOException{
		String line = safeReadln();
		Data data = null;
		List<Double> attribute = null;
		String label = null;
		if(line!=null){
			attribute = new ArrayList<Double>();
			String[] splits = line.split(",");
			int length = splits.length;
			if(splits[splits.length-1].endsWith(";")){												// check data labeled
				length-=1;
				label = splits[splits.length-1];
				label = label.substring(0, label.length()-1);
			}
			for(int i=0;i<length;i++){
				Double d = Double.valueOf(splits[i]);
				attribute.add(d);
			}
			data = new Data(attribute, label);
		}
		return data;
	}
	
//	for(Double attr:this.position)
//		stringBuffer.append(attr + ",");
//	for(String key:this.member.keySet())
//		stringBuffer.append(key +"|"+ this.member.get(key) + ":");
//	stringBuffer.append(this.label + ";");
	
	public Centroid readCentroidln() throws IOException{
		String line = safeReadln();
		Centroid centroid = null;
		if(line!=null){
			centroid = new Centroid();
			String[] splits = line.split(",");
			int length = splits.length;
			if(splits[splits.length-1].endsWith(";")){												// get centroid label and member
				length-=1;																						// remove last index --> info
				String info = splits[splits.length-1];
				info = info.substring(0, info.length()-1);											// remove ";"
				String[] infoSplits = info.split("/");													// separate label and member
				centroid.label = infoSplits[infoSplits.length-1];								// get label
				for(int i=infoSplits.length-2;i>=0;i--){											// get member
					String[] member = infoSplits[i].split(":");
					if(member.length>0)
						centroid.putMember(member[0], Integer.valueOf(member[1]));
				}
			}
			
			List<Double> position = new ArrayList<Double>();							// get position
			for(int i=0;i<length;i++){
				Double d = Double.valueOf(splits[i]);
				position.add(d);
			}
			centroid.position = position;
		}
		return centroid;
	}

	public void close() throws IOException {
		fileReader.close();
	}
}
