package com.nq.utils;

import java.io.*;

import com.nq.data.Centroid;
import com.nq.data.Data;

public class Writer {

	private FileWriter fileWriter = null;
	private BufferedWriter bufferedWriter;

	public Writer(String file) throws IOException{
		this(file, false);
	}

	public Writer (String file, boolean append) throws IOException{
		fileWriter = new FileWriter(file, append);
		bufferedWriter = new BufferedWriter(fileWriter);
    }

	public void writeln(String data) throws IOException {
		bufferedWriter.write(data);
		bufferedWriter.newLine();
		bufferedWriter.flush();
    }

	public void write(String data) throws IOException {
		bufferedWriter.write(data);
		bufferedWriter.flush();
    }
	
	public void writeln(Data data) throws IOException {
		writeln(data.toString());
	}
	
	public void writeln(Centroid centroid) throws IOException {
		writeln(centroid.toString());
	}
	
	public void close() throws IOException {
		fileWriter.close();
//		fileWriter.flush();
	}
}
