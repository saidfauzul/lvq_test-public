package com.nq.main;

import java.io.IOException;

import com.nq.utils.FileName;
import com.nq.utils.Logger;
import com.nq.utils.Reader;
import com.nq.utils.Writer;

public class Merge {
	String[] srcs;
	String dest;
	
	public Merge(String[] args){
		if(args[1].indexOf('*')>=0) {
			try {
				String[] files = FileName.getWildcardFiles(args[1]);
				dest = files[0];
				this.srcs = new String[files.length-1];
				for(int i=0;i<this.srcs.length;i++)
					this.srcs[i] = files[i+1];
			} catch (IOException e) {
				Logger.error("can't get wildcard files");
				Logger.exception(e.getMessage());
			}
		} else if(args[2].indexOf('*')>=0) {
			dest = args[1];
			try {
				String[] files = FileName.getWildcardFiles(args[2]);
				this.srcs = new String[files.length];
				for(int i=0;i<this.srcs.length;i++)
					this.srcs[i] = files[i];
			} catch (IOException e) {
				Logger.error("can't get wildcard files");
				Logger.exception(e.getMessage());
			}
		} else {
			dest = args[1];
			this.srcs = new String[args.length-2];
			for(int i=0;i<this.srcs.length;i++)
				this.srcs[i] = args[i+2];
		}
	}
	
	public boolean start(){
		boolean ok = true;
		Reader reader = null;
		Writer writer = null;
		
		try {
			writer = new Writer(dest, true);										// append file
			for(String src:srcs) {
				reader = new Reader(src);
				while(true){
					String line = reader.readln();
					if(line==null)
						break;
					writer.writeln(line);
				}
				reader.close();														// close for reuse
			}
		} catch (IOException e) {
			ok = false;
			Logger.exception(e.getMessage());
		} finally {
			try {
				writer.close();
				reader.close();
			} catch (IOException e) {
				ok = false;
				Logger.exception(e.getMessage());
			}
		}
		return ok;
	}
	
}
