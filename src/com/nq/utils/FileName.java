package com.nq.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.stream.Stream;

public class FileName {
	 
	 public static String statistics(String dataset, boolean training){
		 dataset = dataset.replace(".data", "");
		 if(!training)
			 dataset = dataset.replace("learning", "training");									// dalam proses learning tetap gunakan data statistics hasil training
		 return dataset + ".stat";
	 }
	 
	 public static String trainingCent(String dataset, boolean training){
		 dataset = dataset.replace(".data", "");
		 if(!training)
			 dataset = dataset.replace("learning", "training");
		 return dataset + "-centroid.train";
	 }
	 
	 public static String learningCent(String dataset, double learningRate){
		 dataset = dataset.replace(".data", "");
		 return dataset + "-" + Formatter.formatFour(learningRate) + "-centroid.learn";
	 }
	 
	 public static String trainingResult(String dataset){
		 dataset = dataset.replace(".data", "");
		 return dataset + "-summary.train";
	 }
	 
	 public static String learningResult(String dataset, double learningRate){
		 dataset = dataset.replace(".data", "");
		 return dataset + "-" + Formatter.formatFour(learningRate) + "-summary.learn";
	 }
	 
	 public static String trainingTemp(String dataset){
		 dataset = dataset.replace(".data", "");
		 return dataset + "-temp.train";
	 }
	 
	 public static String learningTemp(String dataset){
		 dataset = dataset.replace(".data", "");
		 return dataset + "-temp.learn";
	 }
	 
	 public static String sampling(String dataset){
		 dataset = dataset.replace(".csv", "");
		 return dataset + ".data";
	 }
	 
	 public static String[] getWildcardFiles(String wildcard) throws IOException{
		 Object[] files;
		 File dataDir = new File(wildcard);
		 FileSystem fs = FileSystems.getDefault();
		 PathMatcher pathMatcher = fs.getPathMatcher("glob:" + wildcard);
		 FileVisitOption opts = FileVisitOption.FOLLOW_LINKS;
		 try(Stream<Path> stream = Files.walk(dataDir.getParentFile().toPath(), opts)) {
			 files = stream.filter(pathMatcher::matches).toArray();
		 }
		 
		 String[] filesPath = new String[files.length];
		 for(int i=0;i<files.length;i++)
			 filesPath[i] = files[i].toString();
		 
		 return filesPath;
	 }
}