package com.nq.main;

import java.io.IOException;
import com.nq.utils.Logger;
import com.nq.utils.Reader;
import com.nq.utils.Writer;

public class Copy {
	String src, dest, label;
	int startLine, endLine;
	boolean append;
	
	public Copy(String src, String dest, int startLine, int endLine, boolean append){
		this.src = src;
		this.dest = dest;
		this.startLine = Integer.valueOf(startLine).intValue();
		this.endLine = Integer.valueOf(endLine).intValue();
		this.append = Boolean.valueOf(append).booleanValue();
	}
	
	public Copy(String src, String dest, int startLine, int endLine, boolean append, String label){
		this.src = src;
		this.dest = dest;
		this.startLine = Integer.valueOf(startLine).intValue();
		this.endLine = Integer.valueOf(endLine).intValue();
		this.append = Boolean.valueOf(append).booleanValue();
		this.label = label;
	}
	
	public boolean start(){
		boolean ok = true;
		Reader reader = null;
		Writer writer = null;
		try {
			reader = new Reader(src);
			writer = new Writer(dest, append);
//			String head = "MI_dir_L5_weight,MI_dir_L5_mean,MI_dir_L5_variance,MI_dir_L3_weight,MI_dir_L3_mean,MI_dir_L3_variance,MI_dir_L1_weight,MI_dir_L1_mean,MI_dir_L1_variance,MI_dir_L0.1_weight,MI_dir_L0.1_mean,MI_dir_L0.1_variance,MI_dir_L0.01_weight,MI_dir_L0.01_mean,MI_dir_L0.01_variance,H_L5_weight,H_L5_mean,H_L5_variance,H_L3_weight,H_L3_mean,H_L3_variance,H_L1_weight,H_L1_mean,H_L1_variance,H_L0.1_weight,H_L0.1_mean,H_L0.1_variance,H_L0.01_weight,H_L0.01_mean,H_L0.01_variance,HH_L5_weight,HH_L5_mean,HH_L5_std,HH_L5_magnitude,HH_L5_radius,HH_L5_covariance,HH_L5_pcc,HH_L3_weight,HH_L3_mean,HH_L3_std,HH_L3_magnitude,HH_L3_radius,HH_L3_covariance,HH_L3_pcc,HH_L1_weight,HH_L1_mean,HH_L1_std,HH_L1_magnitude,HH_L1_radius,HH_L1_covariance,HH_L1_pcc,HH_L0.1_weight,HH_L0.1_mean,HH_L0.1_std,HH_L0.1_magnitude,HH_L0.1_radius,HH_L0.1_covariance,HH_L0.1_pcc,HH_L0.01_weight,HH_L0.01_mean,HH_L0.01_std,HH_L0.01_magnitude,HH_L0.01_radius,HH_L0.01_covariance,HH_L0.01_pcc,HH_jit_L5_weight,HH_jit_L5_mean,HH_jit_L5_variance,HH_jit_L3_weight,HH_jit_L3_mean,HH_jit_L3_variance,HH_jit_L1_weight,HH_jit_L1_mean,HH_jit_L1_variance,HH_jit_L0.1_weight,HH_jit_L0.1_mean,HH_jit_L0.1_variance,HH_jit_L0.01_weight,HH_jit_L0.01_mean,HH_jit_L0.01_variance,HpHp_L5_weight,HpHp_L5_mean,HpHp_L5_std,HpHp_L5_magnitude,HpHp_L5_radius,HpHp_L5_covariance,HpHp_L5_pcc,HpHp_L3_weight,HpHp_L3_mean,HpHp_L3_std,HpHp_L3_magnitude,HpHp_L3_radius,HpHp_L3_covariance,HpHp_L3_pcc,HpHp_L1_weight,HpHp_L1_mean,HpHp_L1_std,HpHp_L1_magnitude,HpHp_L1_radius,HpHp_L1_covariance,HpHp_L1_pcc,HpHp_L0.1_weight,HpHp_L0.1_mean,HpHp_L0.1_std,HpHp_L0.1_magnitude,HpHp_L0.1_radius,HpHp_L0.1_covariance,HpHp_L0.1_pcc,HpHp_L0.01_weight,HpHp_L0.01_mean,HpHp_L0.01_std,HpHp_L0.01_magnitude,HpHp_L0.01_radius,HpHp_L0.01_covariance,HpHp_L0.01_pcc,label";
//			writer.writeln(head);
			if(endLine<0){																														// copy to end of file
				int readLine = 0;
				while(true){
					String line = reader.readln();
					if(line==null)
						break;
					if(readLine<startLine){
						readLine++;
						continue;
					}
					if(label!=null)
						line = line + "," + label + ";";
					writer.writeln(line);
				}
			} else {
				int readLine = 0;
				while(readLine<=endLine){
					String line = reader.readln();
					if(startLine<=readLine && readLine<=endLine){
						if(line!=null){
							if(label!=null)
								line = line + "," + label + ";";
							writer.writeln(line);
						} else
							Logger.warning("cannot copy line " + readLine + ", value is null");
					}
					readLine++;
				}
			}
		} catch (IOException e) {
			ok = false;
			Logger.exception(e.getMessage());
		} finally {
			try {
				writer.close();
				reader.close();
			} catch (IOException e) {
				ok = false;
				Logger.exception(e.getMessage());
			}
		}
		return ok;
	}
}
